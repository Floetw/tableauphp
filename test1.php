<?php 
//Titre du tableau
echo "Tableau de valeurs jusqu'à 100";

//Déclarer les variables
$nbATester = 0;
$diviseur = 0;

// Déclarer le tableau
$monTableau = array (
    "dizaines" => 0,
    "nbPairs" => 0,
    "nbPremiers" => 0
);



// Boucler pour arriver jusqu'à 100
for ($nbATester = 1; $nbATester <= 100; $nbATester++) {
    //Pour ajouter dans la zone des dizaines
    if (($nbATester%10) == 0) {
        $monTableau["dizaines"]  = $monTableau["dizaines"] + 1;
    }
    //Pour ajouter dans la zone des nombres pairs
    if (($nbATester%2) == 0) {
        $monTableau["nbPairs"]  = $monTableau["nbPairs"] + 1;
    }
    //Pour le cas particulier de 2 (nombre pair et nombre premier)
    if ($nbATester >= 2) {
        $diviseur = 2;
        
        //Pour trouver le premier diviseur
        while (($nbATester%$diviseur) != 0) {
            $diviseur = $diviseur + 1;
        }
        // Si le premier diviseur est le nombre testé (nbATester) alors nbATester est un nombre premier
        if ($diviseur == $nbATester) {
            $monTableau["nbPremiers"]  = $monTableau["nbPremiers"] + 1;
        }
    }
}

// Afficher le tableau
var_dump($monTableau);

?>